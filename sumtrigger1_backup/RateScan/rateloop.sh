#!/bin/bash
# This is a simple shell script
#
# Author: Javier Herrera
# Last updated: 20150430
# Modification: Dazzi => increased the sleep from 4 to 10

while :
do
  ./ratescan.sh
  sleep 10
done

