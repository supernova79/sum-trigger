Procedure for delay zero calibration
++++++++++++++++++++++++++++++++++++
scanning threshold (at zero delay):
-----------------------------------
runmacro initialvalues_Att_FF_zero_delay_4_8_18.uic
runmacro cal_amp_4_8_18.uic

cp .dat	 file to	analysis directory
run analysis (AdjustAplitude.C)
--> Result: flatfielded attenuator settings for zero delay)
cp output table to Cristal directory




scanning the delays (using the flatfielded setting):
----------------------------------------------------
runmacro initialvalues_delay_zero_4_8_18.uic
cal_del_4_8_18.uic

cp .dat  file to        analysis directory
run analysis (AdjustDelay.C)
---> Result: flatfielded times and for flatfielded attenuators (at zero delay)
cp output table		 to Cristal directory


--------------
In CRISTAL:
-----------
1) Set all delays to zero (bias 30V, delay switch off)
2) Load flatfielded attenuators 
3) Load flatfielded delays with function LABSDEL or I_LABSDEL
[4) Save corrected attenuation table using SATT
 5) in the future laod attenuators and delays with LABSDEL and with LATT]





