#!/bin/bash

cd CRISTAL_M1_new

read -r -p "Please check the M1 SumTrigger electronics. Are they on? [y/n]  " response
case "$response" in
  [nN])
    echo "Reset the wall fuse if necessary and restart the electronics from SA!"
    sleep 10
    exit
    ;;
  *)
    echo "Proceeding with CRISTAL startup..."
    sleep 3
    ;;
esac

if ! ps -C cristal  > /dev/null
then
  #for i in `seq 10 -1 1`; do
    #echo "Waiting $i minutes before running CRISTAL..."
    #sleep 60
  #done
  echo "Starting CRISTAL..."
  ./cristal
else
  echo "CRISTAL is already running!"
  sleep 10
fi

exit
