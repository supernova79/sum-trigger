#!/bin/bash
# This is a simple shell script
#
# Author: Javier Herrera
# Last updated: 20150430

LOGFOLD=/media/data/log

LOGFILE=$(ls -rt $LOGFOLD/Cristal*.log | tail -n1)

SUMA1=0
SUMA2=0

SUMA3=0
SUMA4=0

LINES="$(tail -40 $LOGFILE | grep SUM-REPORT)"

echo "-------------------------------------------------------------------------------"
#echo "$LINES"

IFS=$'\n'
for LINE in $LINES; do

  L1RATE=$(echo $LINE | awk '{print $83}')
  L3RATE=$(echo $LINE | awk '{print $84}')
  CONTROL=$(echo $LINE | awk '{print $85}')
  THRESV=$(echo $LINE | awk '{print $86}')

  echo "LINE reads L1RATE=$L1RATE Hz L3RATE=$L3RATE Hz THRESHOLD=$THRESV mV"

  SUMA1=$(awk "BEGIN {print ($SUMA1+$L1RATE); exit}")
  SUMA2=$(awk "BEGIN {print ($SUMA2+$L1RATE*$L1RATE); exit}")

  SUMA3=$(awk "BEGIN {print ($SUMA3+$L3RATE); exit}")
  SUMA4=$(awk "BEGIN {print ($SUMA4+$L3RATE*$L3RATE); exit}")

  #echo "$SUMA1"
  #echo "$SUMA2"
  #echo "$SUMA3"
  #echo "$SUMA4"

done

MEAN1=$(awk "BEGIN {print ($SUMA1)/20; exit}")
VARI1=$(awk "BEGIN {print ($SUMA2/20-$SUMA1/20*$SUMA1/20); exit}")
STDV1=$(awk "BEGIN {print sqrt($VARI1); exit}")

MEAN3=$(awk "BEGIN {print ($SUMA3)/20; exit}")
VARI3=$(awk "BEGIN {print ($SUMA4/20-$SUMA3/20*$SUMA3/20); exit}")
STDV3=$(awk "BEGIN {print sqrt($VARI3); exit}")

echo "L1RATE MEAN=$MEAN1 Hz"
echo "L1RATE STDV=$STDV1 Hz"

echo "L3RATE MEAN=$MEAN3 Hz"
echo "L3RATE STDV=$STDV3 Hz"

