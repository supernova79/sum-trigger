/***************************************************************************/
/*                                                                         */
/* THREADS                                                                 */
/*                                                                         */
/***************************************************************************/
#include "Boole.h"
#include "socklib.h"
#include <string.h>

char *serv_buffer=NULL;
pthread_t serv_tid, client_tid, mon_tid;             /* thread ID structure */
pthread_attr_t serv_attr, client_attr, mon_attr;     /* thread attributes */


//declare a function which is described in cristal.c
void InterpretCC(char *);

void* server_thread(pthread_mutex_t *vme_mutex)
{

  printf ("SERVER TREAD \n");


  SOCKET *sp; 
  int sd;      /* socket descriptor */

  int status, n;
  char str[80];
  time_t timenow; 

  printf ("SERVER server port %d\n",STInfo.portno);

  sleep(1);
  error_logger(__FILE__,ERINFO,"TCP Server thread launched");
  if(serv_buffer==NULL) serv_buffer=(char*)malloc(sizeof(char)*FBUFSIZ);
{
  error_logger(__FILE__,ERINFO,"Server thread: waiting for Central Control to connect");
       printf("waiting for connection\n");

}
  while((sp = sopen()) == NULL) {
    sprintf((char *)str,"Error connecting port number %ld. Retrying in 10 secs...",STInfo.portno);
    printf("Error connecting port number %ld. Retrying in 10 secs...",STInfo.portno);
    error_logger(__FILE__,ERWARN,(char *)str);
    STInfo.icomstatus=ST_COMSTATUS_NOTREADY;
    sleep(10);
  }

  while(!STInfo.exit) 
  {
    STInfo.server_conn=False;
    sprintf((char *)str,"Waiting for connection on port %ld...",STInfo.portno);
    error_logger(__FILE__,ERINFO,(char *)str);
    STInfo.icomstatus=ST_COMSTATUS_READY;
    if ((sd = sserver(sp, STInfo.portno, S_DELAY)) < 0) {
      error_logger(__FILE__,ERINFO,"server connection failed ...");
       printf("server connection failed\n");
      sleep(100);
    }
    else
    {
      error_logger(__FILE__,ERINFO,"Server connection established");
       printf("server connection established\n");
      STInfo.server_conn=True;
      /*
       * Command received: store time and check if there are errors
       * If so, discard the command but flag it as ignored.
       */
      n=0;
      *serv_buffer=0;
      while((n=sreadline(sd, serv_buffer, FBUFSIZ) ) > 0 && !STInfo.exit)
      {


        time(&STInfo.lastcmdtime);
    //   printf("DBG1 server thread on\n");
    //   printf("DBG1 server thread on %i \n",STInfo.istatus);
//       printf("ST_STATUS %i \n",ST_STATUS_ERROR);
       if(STInfo.istatus!=ST_STATUS_ERROR){
     //  printf("DBG2 no status error\n");
          if(STInfo.lock_mode==ST_LOCK)
          {
            AllowInput=0;
            
            if(strncmp(serv_buffer,"CC-REPORT",9)==0)
            {
	    //  printf("DBG3 reading report\n");
  		TERM_GREEN_COLOR;printf("%s",serv_buffer);TERM_RESET_COLOR;
             
              remove_new_line( serv_buffer);
              InterpretCC(serv_buffer);
              error_logger(__FILE__,ERWARN,serv_buffer);
            }
		

            else if(strncmp(serv_buffer,"CAMERA-REPORT",9)==0)
            {
              	TERM_BLUE_COLOR;printf("%s",serv_buffer);TERM_RESET_COLOR;
		//HotPixelsControl(serv_buffer);
	    }
            else 
            {
              	if( strncmp(serv_buffer,"St",2 )== 0 ||strncmp(serv_buffer,"Sum",3)==0 ||strncmp(serv_buffer,"tel",3)==0 ||strncmp(serv_buffer,"Mon",3)==0  || strncmp(serv_buffer,"Exit",4)==0)
		{
			TERM_RED_COLOR;printf("%s",serv_buffer);TERM_RESET_COLOR;
		      	remove_new_line( serv_buffer);
              		if(STInfo.cmdbusy==0)
                	simulate_input(serv_buffer);
			STInfo.icomstatus=ST_COMSTATUS_RECVD;
                	simulate_input(serv_buffer);
			STInfo.SA_command=serv_buffer;
			STInfo.nSA_command++;
			
		}
              	else if(strncmp(serv_buffer,"GRB1",4) == 0)
		{
              		
			int lat_pos =5;
			float latitud_max = 15;
			float latitud_min = -15;
			float latitud ;
			char temp[100];
			
  			strcpy(temp,"");
			
		  int i, k, n;
		  n=0; 
		  for (i=0;i<50;i++)
		  {                                                                              
			    k=0;                                                                         
		    while (strncmp(serv_buffer+i," ",1)!=0 && strcmp(serv_buffer+i,""))
		    {                
		      strncpy(temp+k,serv_buffer+i,1);                                              
//	       printf("tempor: %s \n", temp );                                          
		      k++;          
		      i++;                                                                      
		    }                                                                            
	    n++;                                                                         
	    strcpy(temp+k, "\0");                                                        
	    if (n==lat_pos) 
	    {                                                         
		printf("Lat: %s \n", temp ); 
     		latitud=atof(temp);
     		}	
			
	       }		
			if(STInfo.cmdbusy==0)
			{
			    simulate_input("STOP");
			    simulate_input("MonStop!");
			  if (latitud > latitud_min && latitud < latitud_max)
			  {
			  	simulate_input(" sum Load_th M1_Crab_Thresholds.uic ret"); //uncoment if you want to upload threshold table.i Need to be tested
			  }
			  else
			  {
			  	simulate_input(" sum Load_th M1_extra_Threshold.uic ret"); //uncoment if you want to upload threshold table.i Need to be tested
			   }
			}

     			printf("MAGIC is moving fast, to catch a GRB");     
			STInfo.icomstatus=ST_COMSTATUS_RECVD;
			STInfo.SA_command=serv_buffer;
			STInfo.nSA_command++;
		}
              	else if(strncmp(serv_buffer,"GRB3",4) == 0)
		{
/*			TERM_RED_COLOR;
     printf("%s" ___           ___           __     
    		/\  \         /\  \         /\  \    
   	       /::\  \       /::\  \       /::\  \   
              /:/\:\  \     /:/\:\  \     /:/\:\  \  
             /:/  \:\  \   /::\~\:\  \   /::\~\:\__\ 
            /:/__/_\:\__\ /:/\:\ \:\__\ /:/\:\ \:|__|
            \:\  /\ \/__/ \/_|::\/:/  / \:\~\:\/:/  /
             \:\ \:\__\      |:|::/  /   \:\ \::/  / 
              \:\/:/  /      |:|\/__/     \:\/:/  /  
               \::/  /       |:|  |        \::/__/   
                \/__/         \|__|  );
			TERM_RESET_COLOR;
*/		
              		if(STInfo.cmdbusy==0)
			{
			   //simulate_input("SetTargetRate 30000"); // coment if you want to start from a table
			    simulate_input("MonStart");
			 //    simulate_input(" sum Load_th M1_Crab_Thresholds.uic ret"); //uncoment if you want to upload threshold table.i Need to be tested
			    simulate_input("Start");
			}

			STInfo.SA_command=serv_buffer;
			STInfo.nSA_command++;
     			printf("There we go!");     
		}
            	else 
           	{
		      error_logger(__FILE__,ERWARN,serv_buffer);
         
		 }
	    }
            AllowInput=1;

          }
        }
        else {
	   //   printf("DBG4 ignoring report\n");
          error_logger(__FILE__,ERWARN,"Command Ignored:");
          error_logger(__FILE__,ERWARN,serv_buffer);
          STInfo.icomstatus=ST_COMSTATUS_IGNORED;
        }
      }
    }
    error_logger(__FILE__,ERINFO,"Server connection lost");
  }

  pthread_exit(0);
}

void* client_thread()
{
  int status, n;
  int iclient=0;
  Boolean timerstarted=FALSE;

  sleep(1);
  error_logger(__FILE__,ERINFO,"TCP Client thread launched");
  while(!STInfo.exit) 
  {
     //  printf("DBG con");   
      if(STInfo.client_conn[0]==False && STInfo.hostno[0]>0)
      {
      // printf("DBG connect");   
        client_select(0);
        if(client_connect(STInfo.hostname[0], STInfo.hostno[0]) < 0)
        {
          STInfo.client_conn[0]=False;
        }
        else
        {
          STInfo.client_conn[0]=True;
          char line[100];
          sprintf((char*)line,"Client connection %d established",0); 
          error_logger(__FILE__,ERINFO,line);
          if(timer_on==False && !timerstarted)
          {
            start_timer(imon_sec);
            timerstarted=TRUE;
          }
        }
      }
    sleep(10);
  }

  pthread_exit(0);
}


